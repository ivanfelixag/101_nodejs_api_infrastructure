1. Crear cuenta de AWS
2. Crear usuario en AWS IAM que sea con acceso programático 
3. Crear bucket donde alojar el estado de la infraestructura de terraform
4. Configurar fichero **terraform.config** para que apunte al bucket y a la region
5. Definir las variables de entorno de AWS para que terraform se pueda comunicar con AWS
````
export AWS_ACCESS_KEY_ID=XXXXXXXXXXXX
export AWS_SECRET_ACCESS_KEY=YYYYYYYYYYYYY
export AWS_DEFAULT_REGION=ZZ-ZZZZZ-Z
````
6. Lanzar el comando para inizializar terraform
````
terraform init -backend-config="terraform.config"
````
7. Lanzamos para crear/actualizar la infra
````
terraform apply -var-file="dev.tfvars"
````
8. Cuando queramos destruir la infraestructura
````
terraform destroy -var-file="dev.tfvars"
````

AWS ECS Structure:

```
Cluster
	Service 1 - Target Group 1
		Task 1 - 1
		Task 1 - 2
		Task 1 - 3
	Service 2 - Target Group 2
		Task 2 - 1
		Task 2 - 2
		Task 2 - 3
```

AWS Different modes to implement Service Discovery

https://docs.aws.amazon.com/whitepapers/latest/microservices-on-aws/service-discovery.html

How to implement AWS Service Discovery with Terraform

https://techsparx.com/nodejs/docker/simple-node-service-discovery-ecs-terraform.html

https://medium.com/containers-on-aws/how-to-setup-service-discovery-in-elastic-container-service-3d18479959e6