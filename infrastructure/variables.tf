variable "project_name" {}
variable "environment" {}
variable "region" {}
variable "vpc_cidr" {}
variable "ecs_cluster_name" {}
variable "ecs_service_name" {}
variable "ecs_service_desired_task_number" {}
variable "ecs_service_container_name" {}
variable "ecs_service_task_cpu" {}
variable "ecs_service_task_memory" {}
variable "ecs_service_docker_image_url" {}
variable "ecs_service_docker_container_port" {}
variable "ecs_service_docker_host_port" {}
variable "ecs_service_name_1" {}
variable "ecs_service_desired_task_number_1" {}
variable "ecs_service_container_name_1" {}
variable "ecs_service_task_cpu_1" {}
variable "ecs_service_task_memory_1" {}
variable "ecs_service_docker_image_url_1" {}
variable "ecs_service_docker_container_port_1" {}
variable "ecs_service_docker_host_port_1" {}
variable "availability_zone_1" {
  default = ""
  description = "Availability zone for all of our resources"
}
variable "availability_zone_2" {
  default = ""
  description = "Availability zone to extend the zones of ALB and RDS"
}
variable "public_subnet_availability_zone_1_cidr" {
  default = ""
  description= "Public Subnet CIDR block for the availability zone 1"
}
variable "public_subnet_availability_zone_2_cidr" {
  default = ""
  description= "Public Subnet CIDR block for the availability zone 2 - only used by ALB"
}