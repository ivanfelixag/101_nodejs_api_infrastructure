resource "aws_service_discovery_private_dns_namespace" "service-discovery-internal-private-network" {
  name        = "internal-network"
  description = "Private DNS. All the DNS of the services that use this private DNS end with .internal-network"
  vpc         = aws_vpc.main-vpc.id
}

// #####################################
//            FIRST SERVICE
// #####################################
resource "aws_service_discovery_service" "discovery_service_backend_app" {
  name = "service1"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.service-discovery-internal-private-network.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}

// #####################################
//            SECOND SERVICE
// #####################################
resource "aws_service_discovery_service" "discovery_service_backend_app_1" {
  name = "service2"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.service-discovery-internal-private-network.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}