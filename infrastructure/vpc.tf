// -------- VPC --------
resource "aws_vpc" "main-vpc" {
    cidr_block           = var.vpc_cidr
    enable_dns_hostnames = true              

    tags = {
        Name = "${var.project_name}-${var.environment}-main-VPC"
    }
}

// -------- VPC: Public subnet --------
resource "aws_subnet" "public-subnet-availability-zone-1" {
  cidr_block        = var.public_subnet_availability_zone_1_cidr
  vpc_id            = aws_vpc.main-vpc.id
  availability_zone = var.availability_zone_1

  tags = {
      Name = "${var.project_name}-${var.environment}-public-subnet-az1"
  }
}

resource "aws_subnet" "public-subnet-availability-zone-2" {
  cidr_block        = var.public_subnet_availability_zone_2_cidr
  vpc_id            = aws_vpc.main-vpc.id
  availability_zone = var.availability_zone_2

  tags = {
      Name = "${var.project_name}-${var.environment}-public-subnet-az2"
  }
}

// -------- VPC: Route table for the public zone --------
resource "aws_route_table" "public-route-table" {
  vpc_id            = aws_vpc.main-vpc.id

  tags = {
      Name = "${var.project_name}-${var.environment}-public-route-table"
  }
}

// -------- VPC: Assign public subnet to public route table --------
resource "aws_route_table_association" "public-route-table-availability-zone-1" {
    route_table_id = aws_route_table.public-route-table.id
    subnet_id      = aws_subnet.public-subnet-availability-zone-1.id
}

resource "aws_route_table_association" "public-route-table-availability-zone-2" {
    route_table_id = aws_route_table.public-route-table.id
    subnet_id      = aws_subnet.public-subnet-availability-zone-2.id
}

// -------- Internet Gateway --------
resource "aws_internet_gateway" "internet-gateway" {
  vpc_id = aws_vpc.main-vpc.id

  tags = {
      Name = "${var.project_name}-${var.environment}-internet-gateway"
  }
}

// -------- Internet Gateway: Assign the public route table --------
resource "aws_route" "internet-gateway-route" {
    route_table_id = aws_route_table.public-route-table.id
    gateway_id     = aws_internet_gateway.internet-gateway.id
    destination_cidr_block = "0.0.0.0/0" 
}