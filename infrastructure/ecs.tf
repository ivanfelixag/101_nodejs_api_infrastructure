
// -------- ECS Cluster --------
resource "aws_ecs_cluster" "fargate-cluster" {
  name = var.ecs_cluster_name
}

// -------- Fargate Role --------
// Role that allows access to ECS, ECR, Logs and CloudWatch 
resource "aws_iam_role" "fargate_iam_role" {
  name                = "${var.project_name}-${var.environment}-fargate-iamrole"
  assume_role_policy  = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Effect": "Allow",
    "Principal": {
      "Service": ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    },
    "Action": "sts:AssumeRole"
  }
  ]
}
EOF
}

resource "aws_iam_role_policy" "fargate_iam_role_policy" {
  name    = "${var.project_name}-${var.environment}-fargate-iamrole-policy"
  role    = aws_iam_role.fargate_iam_role.id

  policy  = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ecs:*",
        "ecr:*",
        "logs:*",
        "cloudwatch:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

// #####################################
//            FIRST SERVICE
// #####################################

// -------- ESC Task Definition --------
// Steps to prepare and define the template for the ECS Task
data "template_file" "ecs_task_definition_template" {
  template = file("${path.module}/task_definition.json")

  vars = {
    project_name                      = var.project_name
    environment                       = var.environment
    region                            = var.region
    ecs_service_name                  = var.ecs_service_name
    ecs_service_container_name  = var.ecs_service_container_name
    ecs_service_docker_image_url      = var.ecs_service_docker_image_url
    ecs_service_docker_container_port = var.ecs_service_docker_container_port
    ecs_service_docker_host_port      = var.ecs_service_docker_host_port
  }
}

// ECS task definition
resource "aws_ecs_task_definition" "backend_app_task_definition" {
  container_definitions     = data.template_file.ecs_task_definition_template.rendered
  family                    = var.ecs_service_name
  cpu                       = var.ecs_service_task_cpu
  memory                    = var.ecs_service_task_memory
  requires_compatibilities  = ["FARGATE"]
  execution_role_arn        = aws_iam_role.fargate_iam_role.arn
  task_role_arn             = aws_iam_role.fargate_iam_role.arn

  network_mode = "awsvpc"

  depends_on = [aws_iam_role.fargate_iam_role, aws_cloudwatch_log_group.backend_app_log_group]
}

// -------- APP Security Group --------
// Security group configuration to prepare the inbound and outbound rules
// for our ecs tasks
resource "aws_security_group" "app_security_group" {
  name        = "${var.project_name}-${var.environment}-sg"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.main-vpc.id

  ingress {
    from_port   = var.ecs_service_docker_host_port
    protocol    = "TCP"
    to_port     = var.ecs_service_docker_host_port
    cidr_blocks = ["0.0.0.0/0"]
    security_groups = [aws_security_group.alb-sg.id, aws_security_group.app_security_group_1.id] // ALB only
  }

  egress {
    from_port   = 0 // Allowed all outbound requests
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "${var.ecs_service_name}-SG"
  }
}

// -------- CloudWatch Log --------
// The log configuration specification for the container.
// The ECS task_definition.json need this log
resource "aws_cloudwatch_log_group" "backend_app_log_group" {
  name = "${var.project_name}-${var.environment}-log-group"
}

// -------- ECS Service --------
// Step to create our service from the task definition
resource "aws_ecs_service" "ecs_service" {
  name            = var.ecs_service_name
  task_definition = aws_ecs_task_definition.backend_app_task_definition.arn
  desired_count   = var.ecs_service_desired_task_number
  cluster         = aws_ecs_cluster.fargate-cluster.name
  launch_type     = "FARGATE"

  network_configuration {
    subnets           = [aws_subnet.public-subnet-availability-zone-1.id]
    security_groups   = [aws_security_group.app_security_group.id]
    assign_public_ip  = true
  }

  load_balancer {
    container_name    = var.ecs_service_container_name
    container_port    = var.ecs_service_docker_container_port
    target_group_arn  = aws_alb_target_group.alb_target_group.arn
  }

  service_registries {
      registry_arn = aws_service_discovery_service.discovery_service_backend_app.arn
      container_name = var.ecs_service_container_name
  }

  depends_on = [aws_alb_listener.alb_listener, aws_cloudwatch_log_group.backend_app_log_group]
}

// #####################################
//            SECOND SERVICE
// #####################################

// -------- ESC Task Definition --------
// Steps to prepare and define the template for the ECS Task
data "template_file" "ecs_task_definition_template_1" {
  template = file("${path.module}/task_definition.json")

  vars = {
    project_name                      = var.project_name
    environment                       = var.environment
    region                            = var.region
    ecs_service_name                  = var.ecs_service_name_1
    ecs_service_container_name  = var.ecs_service_container_name_1
    ecs_service_docker_image_url      = var.ecs_service_docker_image_url_1
    ecs_service_docker_container_port = var.ecs_service_docker_container_port_1
    ecs_service_docker_host_port      = var.ecs_service_docker_host_port_1
  }
}

// ECS task definition
resource "aws_ecs_task_definition" "backend_app_task_definition_1" {
  container_definitions     = data.template_file.ecs_task_definition_template_1.rendered
  family                    = var.ecs_service_name_1
  cpu                       = var.ecs_service_task_cpu_1
  memory                    = var.ecs_service_task_memory_1
  requires_compatibilities  = ["FARGATE"]
  execution_role_arn        = aws_iam_role.fargate_iam_role.arn
  task_role_arn             = aws_iam_role.fargate_iam_role.arn

  network_mode = "awsvpc"

  depends_on = [aws_iam_role.fargate_iam_role, aws_cloudwatch_log_group.backend_app_log_group_1]
}

// -------- APP Security Group --------
// Security group configuration to prepare the inbound and outbound rules
// for our ecs tasks
resource "aws_security_group" "app_security_group_1" {
  name        = "${var.project_name}-${var.environment}-1-sg"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.main-vpc.id

  ingress {
    from_port   = var.ecs_service_docker_host_port_1
    protocol    = "TCP"
    to_port     = var.ecs_service_docker_host_port_1
    cidr_blocks = ["0.0.0.0/0"]
    security_groups = [aws_security_group.alb-sg.id] // ALB only
  }

  egress {
    from_port   = 0 // Allowed all outbound requests
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "${var.ecs_service_name_1}-SG"
  }
}

// -------- CloudWatch Log --------
// The log configuration specification for the container.
// The ECS task_definition.json need this log
resource "aws_cloudwatch_log_group" "backend_app_log_group_1" {
  name = "${var.project_name}-${var.environment}-1-log-group"
}

// -------- ECS Service --------
// Step to create our service from the task definition
resource "aws_ecs_service" "ecs_service_1" {
  name            = var.ecs_service_name_1
  task_definition = aws_ecs_task_definition.backend_app_task_definition_1.arn
  desired_count   = var.ecs_service_desired_task_number_1
  cluster         = aws_ecs_cluster.fargate-cluster.name
  launch_type     = "FARGATE"

  network_configuration {
    subnets           = [aws_subnet.public-subnet-availability-zone-1.id]
    security_groups   = [aws_security_group.app_security_group_1.id]
    assign_public_ip  = true
  }

  load_balancer {
    container_name    = var.ecs_service_container_name_1
    container_port    = var.ecs_service_docker_container_port_1
    target_group_arn  = aws_alb_target_group.alb_target_group_1.arn
  }

  service_registries {
      registry_arn = aws_service_discovery_service.discovery_service_backend_app_1.arn
      container_name = var.ecs_service_container_name_1
  }

  depends_on = [aws_alb_listener.alb_listener, aws_cloudwatch_log_group.backend_app_log_group_1]
}