// -------- Application Load Balancer Security Group --------
// Security group to restrict access to your application
// Rememberer this is the external layer
resource "aws_security_group" "alb-sg" {
  name        = "${var.project_name}-${var.environment}-alb-sg"
  description = "Controls access to the ALB"
  vpc_id      = aws_vpc.main-vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// -------- Application Load Balancer --------
// Rememberer this is the external layer
resource "aws_alb" "alb" {
  name            = "${var.project_name}-${var.environment}-alb"
  subnets         = [aws_subnet.public-subnet-availability-zone-1.id, aws_subnet.public-subnet-availability-zone-2.id]
  security_groups = [aws_security_group.alb-sg.id]
}

// -------- Application Load Balancer Listener --------
resource "aws_alb_listener" "alb_listener" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.alb_target_group.arn
    type             = "forward"
  }
}

// #####################################
//            FIRST SERVICE
// #####################################

resource "aws_lb_listener_rule" "static" {
  listener_arn = aws_alb_listener.alb_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/prueba1"]
    }
  }
}

// -------- Application Load Balancer Target Group --------
resource "aws_alb_target_group" "alb_target_group" {
  name        = "${var.project_name}-${var.environment}-alb-tg"
  port        = var.ecs_service_docker_host_port
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main-vpc.id
  target_type = "ip"

  health_check {
    path = "/prueba1"
    matcher = "200"  # has to be HTTP 200 or fails
  }
}

// #####################################
//            SECOND SERVICE
// #####################################

resource "aws_lb_listener_rule" "static_1" {
  listener_arn = aws_alb_listener.alb_listener.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb_target_group_1.arn
  }

  condition {
    path_pattern {
      values = ["/prueba2"]
    }
  }
}

// -------- Application Load Balancer Target Group --------
resource "aws_alb_target_group" "alb_target_group_1" {
  name        = "${var.project_name}-${var.environment}-1-alb-tg"
  port        = var.ecs_service_docker_host_port_1
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main-vpc.id
  target_type = "ip"

  health_check {
    path = "/prueba2"
    matcher = "200"  # has to be HTTP 200 or fails
  }
}
