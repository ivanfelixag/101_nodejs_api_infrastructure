// #####################################
//            FIRST SERVICE
// #####################################

resource "aws_ecr_repository" "default" {
  name = "${var.project_name}"
}

resource "aws_ecr_lifecycle_policy" "ecr_policy_untagged_images" {
  repository = aws_ecr_repository.default.name
  policy     = file("${path.module}/ecr_untagged_policy.json")
}

// #####################################
//            SECOND SERVICE
// #####################################

resource "aws_ecr_repository" "default_1" {
  name = "${var.project_name}_1"
}

resource "aws_ecr_lifecycle_policy" "ecr_policy_untagged_images_1" {
  repository = aws_ecr_repository.default_1.name
  policy     = file("${path.module}/ecr_untagged_policy.json")
}