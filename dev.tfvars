
// Project variables
project_name = "101nodejsapiinfra"
environment = "dev"

// VPC cidrs
vpc_cidr = "10.0.0.0/16"
public_subnet_availability_zone_1_cidr = "10.0.1.0/24"
public_subnet_availability_zone_2_cidr = "10.0.2.0/24"

// ECS config
ecs_service_name = "backend-app"
ecs_service_container_name = "backend-app-task-name"
ecs_service_task_cpu = 256
ecs_service_task_memory = 512
ecs_service_desired_task_number = "1"
ecs_service_docker_container_port = 8000
ecs_service_docker_host_port = 8000

ecs_service_name_1 = "backend-app-1"
ecs_service_container_name_1 = "backend-app-1-task-name"
ecs_service_task_cpu_1 = 256
ecs_service_task_memory_1 = 512
ecs_service_desired_task_number_1 = "1"
ecs_service_docker_container_port_1 = 8000
ecs_service_docker_host_port_1 = 8000