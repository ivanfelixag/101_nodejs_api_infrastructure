module "infrastructure" {
    source              = "./infrastructure"
    project_name        = var.project_name
    environment         = var.environment

    // Region
    region              = local.region
    availability_zone_1 = local.availability_zone_1
    availability_zone_2 = local.availability_zone_2

    // VPC
    vpc_cidr = var.vpc_cidr
    public_subnet_availability_zone_1_cidr  = var.public_subnet_availability_zone_1_cidr
    public_subnet_availability_zone_2_cidr  = var.public_subnet_availability_zone_2_cidr

    // ECS
    ecs_cluster_name                    = local.ecs_cluster_name
    
    ecs_service_name                    = var.ecs_service_name
    ecs_service_container_name    = var.ecs_service_container_name
    ecs_service_task_cpu                = var.ecs_service_task_cpu
    ecs_service_task_memory             = var.ecs_service_task_memory
    ecs_service_desired_task_number     = var.ecs_service_desired_task_number
    ecs_service_docker_image_url        = local.ecs_service_docker_image_url
    ecs_service_docker_container_port   = var.ecs_service_docker_container_port
    ecs_service_docker_host_port        = var.ecs_service_docker_host_port

    ecs_service_name_1                    = var.ecs_service_name_1
    ecs_service_container_name_1    = var.ecs_service_container_name_1
    ecs_service_task_cpu_1                = var.ecs_service_task_cpu_1
    ecs_service_task_memory_1             = var.ecs_service_task_memory_1
    ecs_service_desired_task_number_1     = var.ecs_service_desired_task_number_1
    ecs_service_docker_image_url_1        = local.ecs_service_docker_image_url_1
    ecs_service_docker_container_port_1   = var.ecs_service_docker_container_port_1
    ecs_service_docker_host_port_1        = var.ecs_service_docker_host_port_1
}