variable "aws_account_id" {}
variable "aws_region" {}
variable "project_name" {}
variable "environment" {}
variable "vpc_cidr" {}
variable "ecs_service_name" {}
variable "ecs_service_desired_task_number" {}
variable "ecs_service_container_name" {}
variable "ecs_service_task_cpu" {}
variable "ecs_service_task_memory" {}
variable "ecs_service_docker_container_port" {}
variable "ecs_service_docker_host_port" {}
variable "ecs_service_name_1" {}
variable "ecs_service_desired_task_number_1" {}
variable "ecs_service_container_name_1" {}
variable "ecs_service_task_cpu_1" {}
variable "ecs_service_task_memory_1" {}
variable "ecs_service_docker_container_port_1" {}
variable "ecs_service_docker_host_port_1" {}
variable "availability_zone_1" {
  default = ""
  description = "Availability zone for all of our resources"
}
variable "availability_zone_2" {
  default = ""
  description = "Availability zone to extend the zones of ALB and RDS"
}
variable "public_subnet_availability_zone_1_cidr" {
  default = ""
  description= "Public Subnet CIDR block for the availability zone 1"
}
variable "public_subnet_availability_zone_2_cidr" {
  default = ""
  description= "Public Subnet CIDR block for the availability zone 2 - only used by ALB"
}


locals {
  region = var.aws_region
  availability_zone_1 = "${var.aws_region}a"
  availability_zone_2 = "${var.aws_region}b"
  ecs_cluster_name = "${var.project_name}-${var.environment}"

  ecs_service_docker_image_url = "${var.aws_account_id}.dkr.ecr.${var.aws_region}.amazonaws.com/${var.project_name}-${var.environment}"
  ecs_service_docker_image_url_1 = "${var.aws_account_id}.dkr.ecr.${var.aws_region}.amazonaws.com/${var.project_name}-1-${var.environment}"
}
